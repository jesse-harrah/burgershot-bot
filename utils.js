const fs = require('fs');
const Discord = require('discord.js');
const { Message } = require('discord.js');
const config = require('./config.json');

function getUserFromMention(mention, client) {
	if (!mention) return;

	if (mention.startsWith('<@') && mention.endsWith('>')) {
		mention = mention.slice(2, -1);

		if (mention.startsWith('!')) {
			mention = mention.slice(1);
		}

		return client.users.cache.get(mention);
	}
}

function banUser(user, guild) {
	guild.members.ban(user);
}

function unbanUser(id, guild) {
	guild.members.unban(id)
}

function reactionCollector(callback, error) {
	const filter = (reaction, user) => reaction.emoji.name === '👍' && user.id === Message.author.id;

	Message.awaitReactions(filter, { max: 4, time: 60000, errors: [ 'time' ]})
		.then(collected => {
			console.log({ size: collected.size, collected });
			if (callback) callback(collected);
		}).catch(collected => {
			if (error) {
				error(collected);
			} else console.log(`After a minute, only ${collected.size} out of 4 reacted.`)
		});
}

function setBotStatus(client, status, type = 'PLAYING') {
    if (status) client.user.setActivity(status, { type: type });
}

function setupEvents(client) {
    const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('.js'));
    for (const file of eventFiles) {
        const event = require(`./events/${file}`);
        if (event.once) {
            client.once(event.name, (...args) => event.execute(...args, client));
        } else {
            console.log(`Setting up ${event.niceName || event.name} for 'on'.`);
            client.on(event.name, (...args) => event.execute(...args, client));
        }
    }
}

function setupCommands(client) {
    client.commands = new Discord.Collection();
    client.cooldowns = new Discord.Collection();

    const commandFolders = fs.readdirSync(`./commands`);
    // Add commands to collection
    for (const folder of commandFolders) {
        const commandFiles = fs.readdirSync(`./commands/${folder}`).filter(file => file.endsWith(`.js`));
        for (const file of commandFiles) {
            const command = require(`./commands/${folder}/${file}`);
            if (command.dontLoad) continue;
            console.info(`Adding command ~~~~~~~ '${command.name}'.`);
            client.commands.set(command.name, command);
            if (command.aliases) {
                for (const alias of command.aliases) {
                    console.info(`Adding alias ~~~~~~~~~ '${alias}'.`);
                    client.commands.set(alias, command);  
                }
            }
        }
    }
}

function truncateStr(str, maxLength) {
    return str.substr(0, maxLength);
}

function updateStashMsg(strings, ...args) {
    `${userData.niceName ? userData.niceName : userData.name}
     ${amount >= 0 ? 'added' : 'removed'} \`${amount} x ${item}\` to the stash.`
    return 'hello';
}


module.exports = {
    config,
    getUserFromMention,
	banUser,
	unbanUser,
	reactionCollector,
	setBotStatus,
	setupEvents,
	setupCommands,
    truncateStr
};



// const Discord = require('discord.js');
// const client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });
// client.on('messageReactionAdd', async (reaction, user) => {
// 	// When a reaction is received, check if the structure is partial
// 	if (reaction.partial) {
// 		// If the message this reaction belongs to was removed, the fetching might result in an API error which should be handled
// 		try {
// 			await reaction.fetch();
// 		} catch (error) {
// 			console.error('Something went wrong when fetching the message: ', error);
// 			// Return as `reaction.message.author` may be undefined/null
// 			return;
// 		}
// 	}
// 	// Now the message has been cached and is fully available
// 	console.log(`${reaction.message.author}'s message "${reaction.message.content}" gained a reaction!`);
// 	// The reaction is now also fully available and the properties will be reflected accurately:
// 	console.log(`${reaction.count} user(s) have given the same reaction to this message!`);
// });