const gKeys = require('./google-keys2.json');
const { token, firebaseKeys } = require('./config.json');
const Discord = require('discord.js');
const disbut = require('discord-buttons');
const { setupEvents, setupCommands } = require('./utils');
const { google } = require('googleapis');
const firebase = require('firebase');

let jwtClient = new google.auth.JWT(
    gKeys.client_email,
    null,
    gKeys.private_key,
    [
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/youtube'
    ]);

//authenticate request
jwtClient.authorize(function (err, tokens) {
    if (err) {
        console.log(err); return;
    } else { console.log("Successfully connected to google Api-s!"); }
});

const app = firebase.initializeApp(firebaseKeys);

const client = new Discord.Client({ partials: [ 'MESSAGE', 'CHANNEL', 'REACTION']});
disbut(client);
setupEvents(client);
setupCommands(client);

// client
//   .on("debug", console.log)
//   .on("warn", console.log)

client.login(token).then(res => { 
    console.log('client logged in'); 
    client.google = {};
    client.google.jwtClient = jwtClient;
    client.google.auth = jwtClient;
}).catch(error => console.error('error', error));