/** ONCE READY */

module.exports = {
	name: 'unhandledRejection',
    niceName: 'Unhandled Rejection',
	execute(error) {
        console.error('Unhandled promise rejection:', error);
	},
};