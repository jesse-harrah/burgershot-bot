/**
 * Event based on message send, if Application Webhook then we react to the message.
 */

const REACTIONS = [
    '✅',
    '❌'
];

const WEBHOOK_ID = '896868706834083850';

/** COMMAND HANDLING */
module.exports = {
	name: 'message',
    niceName: 'Auto-React',
	async execute(message, client) {
        const { webhookID } = message;
        if (webhookID === WEBHOOK_ID) {
            for await (const emoji of REACTIONS)
                await message.react(emoji);
        }
    }
};
