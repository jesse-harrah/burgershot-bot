/** ON MESSAGE */

module.exports = {
	name: 'message',
    niceName: 'Logging',
	execute(message) {
		console.log(`${message.author.tag} in #${message.channel.name} sent: ${message.content}`);
	},
};