/** ONCE READY */

module.exports = {
	name: 'ready',
    niceName: 'Ready',
	once: true,
	execute(client) {
        console.log(`Bot Server Ready! Logged in as ${client.user.tag}`);
        client.user.setActivity('with Firestore and Discord.js :)', { type: 'PLAYING' });
	},
};
