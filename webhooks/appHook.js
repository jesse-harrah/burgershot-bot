const POST_URL = 'https://discord.com/api/webhooks/892333257616683008/mfnYXn7WkeIqUome-P4NicAAQgvlBcdxUiA3Zi6Y1_8BnMFiOYTS-auAzD4mn-vluVb7';
const BS_POST_URL = 'https://discord.com/api/webhooks/896868706834083850/EHqZ2frQwHKJ9oxlyJ_txwo7Dhh-99JmL5xMnyGpS69F5WEGueNklR1hSJGn6PyC3aRv';

const TITLE = 'Burger Shot Applications';
const FOOTER_TEXT = `✅ If you think we should hire them, ❌ If you think we should skip them`;

function postAllApplications() {
  const form = FormApp.getActiveForm();
    const allResponses = form.getResponses();
    console.log('allResponses', allResponses);

    allResponses.forEach((response, i) => {

      const items = getMessage(response.getItemResponses());
      const name = items && items[0] ? items[0].value : '';
      const discord = items && items[1] ? items[1].value : '';

      const options = {
        'method': 'post',
        'headers': {
            'Content-Type': 'application/json',
        },
        'payload': JSON.stringify({
            'content': `**${name} | ${discord}**`, // This is not an empty string
            'embeds': [{
                'title': TITLE,
                'fields': items,
                'footer': {
                  'text': FOOTER_TEXT + ' | ' + (new Date()).toDateString()
                }
            }]
        })
      };

      UrlFetchApp.fetch(BS_POST_URL, options);
    });
}
// postAllApplications()

function onSubmit(e) {
    const form = FormApp.getActiveForm();
    const allResponses = form.getResponses();
    console.log('allResponses', allResponses);
    const latestResponse = allResponses[allResponses.length - 1];

    const response = latestResponse.getItemResponses();

    const items = getMessage(response);
    const name = items && items[0] ? items[0].value : '';
    const discord = items && items[1] ? items[1].value : '';

    const options = {
      'method': 'post',
      'headers': {
          'Content-Type': 'application/json',
      },
      'payload': JSON.stringify({
          'content': `**${name} | ${discord}**`, // This is not an empty string
          'embeds': [{
              'title': TITLE,
              'fields': items,
              'footer': {
                'text': FOOTER_TEXT + ' | ' + (new Date()).toDateString()
              }
          }]
      })
    };

    UrlFetchApp.fetch(BS_POST_URL, options);
    UrlFetchApp.fetch(POST_URL, options);

};

function getMessage(response) {
  const items = [];
  for (let i = 0; i < response.length; i++) {
    const question = response[i].getItem().getTitle();
    const answer = response[i].getResponse();
    let parts = [];
    try {
        parts = answer.match(/[\s\S]{1,1024}/g) || [];
    } catch (e) {
        parts = answer;
    }

    if (answer == '') { continue; }
    for (let j = 0; j < parts.length; j++) {
        if (j == 0) {
            items.push({
                'name': question,
                'value': parts[j],
                'inline': false
            });
        } else {
            items.push({
                'name': question.concat(' (cont.)'),
                'value': parts[j],
                'inline': false
            });
        }
    }
  }
  return items;
}