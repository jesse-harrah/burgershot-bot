const { setBotStatus } = require('../../utils');

module.exports = {
	name: 'setbotstatus',
    aliases: [ 'botstatus' ],
	description: 'Set the status for the bot.',
    usage: '<statusMessage>',
    args: true,
    tagUser: false,
    cooldown: 5,
    prune: true,
    permissions: 'ADMINISTRATOR',
    async execute(message, args, client) {
        const VALID_STATUSES = [
            'PLAYING', 'STREAMING',
            'LISTENING', 'WATCHING',
            'COMPETING'
        ];
        const [ first, ...rest ] = args;
        if (VALID_STATUSES.includes(first.toUpperCase())) {
            const type = String(args.shift()).toUpperCase() || 'PLAYING';
            let msg;
            msg = args.length ? msg = args.join(` `) : ` `;
            setBotStatus(client, msg, type);
            let res = await message.reply(`successfully set bot status to: ${type} ${msg}`);
            await res.delete({ timeout: 10000 });
        } else {
            let msg;
            msg = args.length ? msg = args.join(` `) : ` `;
            setBotStatus(client, msg);
            let res = await message.reply(`successfully set bot status to: ${msg}`);
            await res.delete({ timeout: 10000 });
        }
    }
};