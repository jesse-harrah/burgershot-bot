const { setNiceName } = require('./firebaseHelpers');

module.exports = {
    name: 'setnicename',
    aliases: [],
    description: 'Add items to the stash.',
    usage: '<niceName>',
    args: true,
    tagUser: false,
    cooldown: 0,
    prune: true,
    execute(message, args, client) {
        setNiceName(message.author.id, args.join(' '));
        // message.reply(`Succesfully set your Nice Name to: \`${args.join(' ')}\``);

    }
};
