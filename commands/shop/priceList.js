const { getDoc, getUserByName, sendEmbed, getItems, headers } = require('./firebaseHelpers');
const { getUserFromMention } = require('../../utils');

module.exports = {
    name: 'pricelist',
    aliases: [],
    description: 'Check items in the stash.',
    usage: '[user]',
    args: false,
    tagUser: false,
    cooldown: 0,
    prune: true,
    execute(message, args, client) {
        const user = getUserFromMention(args[0], client);
        console.log('user', user);

        getItems().then(doc => {
            console.log('doc vals', doc);
            const data = doc;
            const space =  { name: '\u200B', value: '\u200B' };

            const embed = {
                title: 'Burger Shot',
                // thumbnail: { url: `https://i.gyazo.com/57dbe9e40c74555d7cf613e69bbc3580.png` },
                fields: [  ],
                timestamp: new Date(),
                footer: { 
                    text: `Coded with ❤ • Last updated ${new Date(data.lastUpdated).toLocaleTimeString()}`
                }
            };

            let c = 1;
            for (const item of data) {
                if (item.name === 'test') continue;
                if (c % 2) embed.fields.push(space);
                c++;
                const price = item.price;
                embed.fields.push({
                    name: headers[item.name],
                    value: `Sell Price: ${price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        Buy Price: ${item.purchase.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        Stash: ${item.quantity}`,
                    inline: true
                });
            }
            embed.fields.push(space);
            message.channel.send({ embed });
        });
    }
};
