const { updateDoc, getDoc, ITEMS } = require('./firebaseHelpers');

module.exports = {
    name: 'add',
    aliases: [],
    description: 'Add items to the stash.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    prune: true,
    async execute(message, args, client) {

        if (String(args[0]).toUpperCase() in ITEMS) {
            const item = ITEMS[String(args[0]).toUpperCase()];

            const amount = parseInt(args[1]);
            if (isNaN(amount)) {
                return message.reply('That\'s not a valid number.');
            } if (amount <= 0) {
                return message.reply(`\`amount\` must be greater than 0.`);
            }

            try {
                const userRes = await updateDoc(message, { [ item ]: amount });
                const userDoc = await getDoc(userRes.docId);
                const userData = userDoc.data();
                await updateDoc(message, { quantity: amount }, 'items', item);

                message.channel.send(`${userData.niceName ? userData.niceName : userData.name} ${amount >= 0 ? 'added' : 'removed'} \`${amount} x ${item}\` to the stash.`);
            } catch (err) {
                message.reply('Unable to update stash:', err.message ? err.message : err);
            }
            
        } else return message.reply(`${args[0]} is not a valid item. Valid items: ${Object.keys(ITEMS).map(s => `\`${s.toLocaleLowerCase()}\``).join(' ')}.`);
    }
};
