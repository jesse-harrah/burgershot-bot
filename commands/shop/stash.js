const { getDoc, getUserByName, sendEmbed } = require('./firebaseHelpers');
const { getUserFromMention } = require('../../utils');

module.exports = {
    name: 'stash',
    aliases: [],
    description: 'Check items in the stash.',
    usage: '[user]',
    args: false,
    tagUser: false,
    cooldown: 0,
    prune: true,
    execute(message, args, client) {
        if (args.length) {
            const user = getUserFromMention(args[0], client);
            console.log('user', user);
            getUserByName(user.username).then(snapshot => {
                let c = false;
                snapshot.forEach(doc => {
                    if (c) return;
                    sendEmbed(message, doc);
                    c = true;
                });
                if (!c) {
                    message.channel.send(`Error finding user for name: \`${user.username}\``);
                }
            }).catch(error => {
                message.channel.send(`Error finding user for name: \`${args.join(' ')}\`` + error.message ? error.message : error);
            });
        } else {
            getDoc(message.author.id)
            .then(doc => {
                sendEmbed(message, doc);
            }).catch(console.error);
        }
    }
};
