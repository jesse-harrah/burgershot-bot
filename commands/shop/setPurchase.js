const { updateDoc, getDoc, ITEMS } = require('./firebaseHelpers');

module.exports = {
    name: 'setpurchase',
    aliases: [],
    description: 'Add items to the stash.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    prune: true,
    permissions: 'ADMINISTRATOR',
    execute(message, args, client) {

        if (String(args[0]).toUpperCase() in ITEMS) {
            const item = ITEMS[String(args[0]).toUpperCase()];

            const amount = parseInt(args[1]);
            if (isNaN(amount)) {
                return message.reply('That\'s not a valid number.');
            }

            updateDoc(message, {
                purchase: amount
            }, 'items', item).then(_ => { 
                return getDoc(item, 'items');
            }).then(doc => {
                console.log('doc vals', doc.data());
                const data = doc.data();
                console.log('itemdata', data);
                message.channel.send(`${message.author.username} updated purchase price for \`${item}\` to price: ${amount}.`);
            }).catch(err => {
                console.log('error!', err);
                message.reply('Unable to update stash:', err.message ? err.message : err);
            });
            // console.log('done', done);
        } else return message.reply(`${args[0]} is not a valid item. Valid items: ${Object.keys(ITEMS).map(s => `\`${s.toLocaleLowerCase()}\``).join(' ')}.`);
    }
};