const { updateDoc, getItemByName, ITEMS } = require('./firebaseHelpers');

module.exports = {
    name: 'sell',
    aliases: [],
    description: 'Sell items from the stash.',
    usage: '<item> <quantity>',
    args: true,
    tagUser: false,
    cooldown: 0,
    prune: true,
    async execute(message, args, client) {

        if (String(args[0]).toUpperCase() in ITEMS) {
            const item = ITEMS[String(args[0]).toUpperCase()];

            const amount = parseInt(args[1]);
            if (isNaN(amount)) {
                return message.reply('That\'s not a valid number.');
            } if (amount <= 0) {
                return message.reply(`\`amount\` must be greater than 0.`);
            }
            try {
                const itemsSnapshot = await getItemByName(item);
                let c = false;
                let data;
                itemsSnapshot.forEach(async doc => {
                    data = doc.data();
                    console.log(data);
                    if (c) return;
                    itemDoc = await updateDoc(message, { 
                        [ item ]: -amount,
                        ['cash']: data.price * amount,
                     });

                    let stashRes = await updateDoc(message, { quantity: -amount }, 'items', item);
                    let stashRes2 = await updateDoc(message, { quantity: data.price * amount }, 'items', 'cash');
                });

                message.channel.send(`Removed \`${amount} x ${item}\` from the stash.
Added \`${data.price * amount} x ${'money'}\` to the stash.`);
            } catch(err) {
                console.log('error!', err);
                message.reply('Unable to update stash:', err.message ? err.message : err);
            }
        } else return message.reply(`${args[0]} is not a valid item. Valid items: \`scrap\`, \`kits\`, \`cash\`, \`dirty\`, \`grapes\`.`);
    }
};
